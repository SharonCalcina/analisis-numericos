#En una palabra de 16 bits representar las siguientes funciones con su respectivo error relativo porcentual.
import math
def relativeError(number):
    numeros=str(number)
    nuevacadena="0."
    c=0
    c2=0
    potencia=0
    numero=str(number)
    if numero[0]=='-':
        number=float(transform(number)) 
    s=str(conversion_fraccion_10_2(number))
    tamano=len(s)
    for i in range (1,tamano):
        c=c+1
        if s[i]==".":
            if c>1:
                potencia=c
            else: 
                if s[i-1]!="0":
                    potencia=1
                else:
                    j=i+1
                    flag=0
                    while flag==0:
                        if s[j]=="0":
                            c2=c2+1
                            j=j+1
                        else:
                            flag=1    
                    potencia=c2

    nuevacadena+=nuevoString2(number)
    binario=binario_decimal(nuevoString2(number))
    nuevonum=binario*(2**potencia)
    
    if numeros[0]=="-":
        print("NUMERO ORIGINAL: -",number)
        print("NUM ORIGINAL CONVERTIDO A BINARIO: -",s)
        print("CONVIRTIENDO A DECIMAL: -",binario,'*2^',int(potencia))
        potencia=bin(potencia)
        pot,potencia=potencia.split("b")
        print("PASANDO A UN NUMERO: -"+nuevacadena,'*2^',int(potencia))
        print("CALCULANDO: -",nuevonum)
        print("CALCULANDO EL VALOR RELATIVO: -",math.fabs((number-nuevonum)/number)*100)
    else:
        print("NUMERO ORIGINAL: ",number)
        print("NUM ORIGINAL CONVERTIDO A BINARIO: ",s)
        print("CONVIRTIENDO A DECIMAL: ",binario,'*2^',potencia)
        potencia=bin(potencia)
        pot,potencia=potencia.split("b")
        print("PASANDO A UN NUMERO: "+nuevacadena,'*2^',int(potencia))
        print("CALCULANDO: ",nuevonum)
        print("CALCULANDO EL VALOR RELATIVO: ",math.fabs((number-nuevonum)/number)*100)
    
def binario_decimal (binary):
    decimal=int(0)
    for i in range (0,len(binary)): 
        decimal=decimal+(int(binary[i])*pow(2,-(i+1)))
    return decimal

def nuevoString(cad):
    nuevacadena=""
    cadena=str(conversion_fraccion_10_2(cad))
    length=len(cadena)
    k=0

    while cadena[k]=="0" or cadena[k]==".":
        k += 1 
    for m in range (k,length):
        nuevacadena+=cadena[m]
    return nuevacadena
def nuevoString2(cad):
    nuevacadena=""
    cadena=str(conversion_fraccion_10_2(cad))
    length=len(cadena)
    k=0
    for m in range (k,length):
        if(cadena[m]=="."):
            nuevacadena+=""
        else:
            nuevacadena+=cadena[m]
    return nuevacadena
def conversion_fraccion_10_2(x):
    n=0.0
    d=0
    res=""
    m=1
    res1=""
    decimal,entero=math.modf(x)
    byte=0
    resultado=""
    while byte<=8:
        n=decimal*2
        d,m=math.modf(n)
        #print(n,d,m)
        decimal=d 
        byte=byte+1      
        res=res+str(int(m))
    res=res+str(int(m))
    if entero>=2 : 
        ent=entero
        while ent>=2:
            d=int(ent/2)
            res1=res1+str(int(ent%2))
            ent=d     
        res1=res1+str(ent)
        resultado=res1[::-1]+"."+res
        return resultado
    else:
        resultado=str(int(entero))+"."+res
        return resultado
def transform(numero):
    numero=str(numero)
    numero2=""
    for p in range(1,len(numero)):
        numero2+=numero[p]
    return numero2
#V =π(D^2)h/4
#D = 15.5[cm] ∆D = 0.3[cm]
#h = 13.2[cm] ∆h = 0.2[cm]
def volumen(D, h):
    volumen=(math.pi*pow(D,2)*h)/4
    return volumen
def mantisa(cadena):
    nuevacadena=""
    for m in range (0,8):
        nuevacadena+=cadena[m]
    return nuevacadena
def propag_error(number):
    numeros=str(number)
    nuevacadena="0."
    c=0
    c2=0
    potencia=0
    numero=str(number)
    if numero[0]=='-':
        number=float(transform(number)) 
    s=str(conversion_fraccion_10_2(number))
    tamano=len(s)
    for i in range (1,tamano):
        c=c+1
        if s[i]==".":
            if c>1:
                potencia=c
            else: 
                if s[i-1]!="0":
                    potencia=1
                else:
                    j=i+1
                    flag=0
                    while flag==0:
                        if s[j]=="0":
                            c2=c2+1
                            j=j+1
                        else:
                            flag=1    
                    potencia=c2

    nuevacadena+=nuevoString2(number)
    print(nuevoString2(number))
    potencia=bin(potencia)
    pot,potencia=potencia.split("b")
    if numeros[0]=="-":
        print("signo: 1")
        print("exponente: ",int(potencia))
        print("numero original convertido a binario: ",s)
        print("mantisa: ",mantisa(nuevoString2(number)))

    else:
        print("signo: 0")
        print("exponente: ",int(potencia))
        print("numero original convertido a binario: ",s)
        print("mantisa: ",mantisa(nuevoString2(number)))
print("----------------------        ANALISIS DE ERRORES        ------------------")
print("/////////////////EJERCICIO 1 ---- ERROR RELATIVO PORCENTUAL////////////////")
relativeError(0.05)
print("----------------------        ANALISIS DE ERRORES        ------------------")
print("///////////////////////EJERCICIO 2 ---- COMA FLOTANTE//////////////////////")
propag_error(volumen(15.5,13.2))
#propag_error(-125.32)

