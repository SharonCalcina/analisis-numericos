import math

#f(x) = 5x^3 − 5x^2 + 6x − 2

def biseccion(a, b, error, funcion):
    fun_a=split(a,funcion)
    fun_b=split(b,funcion)
    if(fun_a*fun_b)<0:
        error2=float(3.0)
        c=0
        count=1
        cant=float((a+b)/2)
        print("dellll",cant)
        while error2>error:
            print("-------------------    Iteracion ",count,"   -------------------")
            c=float((a+b)/2)
            print("a: ",a)
            print("b: ",b)
            print("c: ",c)
            fa=split(a,funcion)
            fb=split(b,funcion)
            fc=split(c,funcion)
            print("fa: ",fa)
            print("fb: ",fb)
            print("fc: ",fc)
            print("fa*fc:  ",fa*fc)
            print("'c'  anterior",cant)
            if (fa*fc)<0:
                b=c
            if (fa*fc)>0:
                a=c
            if (fa*fc)==0:
                print(c)
                error2=0
            cnew=float((a+b)/2)
            print("'c'  nuevo: ",cnew)
            if cnew==0:
                error2=0
            else:
                error2=math.fabs((cnew-cant)/cnew)*100
            print("error  ",error2)
            cant=cnew
            count+=1
        print("raiz: ",c)
    
def eval_fun(x,funcion):
    result=""
    for i in funcion:
        if i =="x":
            i=str(x)           
        result+=i
    return result
def split (x,result):
    last_figure=0
    cifra=""
    for i in result:
        if (i == "+" or i == "-"):
            cifra =eval_fun(x,cifra)
            #print("cifra------",cifra)
            last_unit=0
            first=""
            second=""
            exp=""
            s=0
            param=""
            for j in cifra:
                if j== "*" or j=="^":
                    param+=j
            #print(param)
            if param=="*^":
                while cifra[s]!="*":
                    first+=cifra[s]
                    s+=1
                #print(first)
                s+=1
                while cifra[s]!="^":
                    second+=cifra[s]
                    s+=1
                #print(second)
                s+=1
                while s!=len(cifra):
                    exp+=cifra[s]
                    s+=1
                #print(exp)
                last_unit+=(float(first)*pow(float(second),float(exp)))
                #print (last_unit)
            if param=="*":
                while cifra[s]!="*":
                    first+=cifra[s]
                    s+=1
                #print(first)
                s+=1
                while s!=len(cifra):
                    second+=cifra[s]
                    s+=1
                #print(second)
                last_unit+=(float(first)*float(second))
                #print (last_unit)
            if param=="^":
                while cifra[s]!="^":
                    first+=cifra[s]
                    s+=1
                #print(first)
                s+=1
                while s!=len(cifra):
                    second+=cifra[s]
                    s+=1
                #print(second)
                last_unit+=(pow(float(first),float(second)))
                #print (last_unit)
            if param =="":
                last_unit=float(cifra)
                #print(last_unit)
            last_figure+=last_unit
            cifra=""    
        if i=="-":
            cifra+="-"

        else:
            cifra+=i
    
    first=""
    second=""
    exp=""
    param=""
    last_unit=0
    #print("cifra", cifra)
    for jj in cifra:
        if jj== "*" or jj=="^":
            param+=jj
    #print(param)
    if param=="*^":
        while cifra[s]!="*":
            first+=cifra[s]
            s+=1
        #print(first)
        s+=1
        while cifra[s]!="^":
            second+=cifra[s]
            s+=1
        #print(second)
        s+=1
        while s!=len(cifra):
            exp+=cifra[s]
            s+=1
        #print(exp)
        last_unit+=(float(first)*pow(float(second),float(exp)))
        #print(last_unit)
    if param=="*":
        while cifra[s]!="*":
            first+=cifra[s]
            s+=1
        #print(first)
        s+=1
        while s!=len(cifra):
            second+=cifra[s]
            s+=1
        #print(second)
        last_unit+=(float(first)*float(second))
        #print (last_unit)
    if param=="^":
        while cifra[s]!="^":
            first+=cifra[s]
            s+=1
        #print(first)
        s+=1
        while s!=len(cifra):
            second+=cifra[s]
            s+=1
        #print(second)
        last_unit+=(pow(float(first),float(second)))
        #print (last_unit)
    else:
        last_unit=float(cifra)  
    last_figure+=last_unit
    #print("cifra final",last_figure)  
    return last_figure 
         
#biseccion(0,2,0.0005,"5*x^3-5*x^2+6*x-2")

print("/////////////////////////////////")
#biseccion(0,1,1,"x^4+3*x^3-2")

#NEWTON RAPHSON NUMERIC METHOD
#f(x) = 2x3 −11.7x2 + 17.7x−5 
#x0 = 3 
#error = 0.001
def funcion(x):
    return 2*x**3-11.7*x**2+17.7*x-5
def derivada(x):
    return 6*x**2-11.7*2*x+17.7-5
def newton_raphson(x0,error):
    error2=10.0
    i=1
    while(error2>error):
        print("-----------------------  Iteracion  ",i," -----------------------")
        x1=x0-(funcion(x0)/derivada(x0))
        error2=math.fabs(x1-x0)
        x0=x1
        print("error:  ",error2)
        print("raiz aproximada:   ",x0)
        i+=1

newton_raphson(3,0.001)
